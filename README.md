# Discord RPC Maker

<p align="left">
<a href="https://twitter.com/bymnkn" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/twitter.svg" alt="bymnkn" height="30" width="40" /></a>
<a href="https://instagram.com/mnkn.x" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/instagram.svg" alt="mnkn.x" height="30" width="40" /></a>
<a href="https://www.youtube.com/c/bymnknn" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/youtube.svg" alt="bymnknn" height="30" width="40" /></a>
<a href="https://discordapp.com/users/290675883784667136" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/discord.svg" alt="bymnkn#9999" height="30" width="40" /></a>
</p>

# Gerekli Programlar
Node https://nodejs.org/en/

Git https://git-scm.com/download/win

NotePad++ https://notepad-plus-plus.org/download/

Discord bot https://discordapp.com/developers/

config.json
```` 
 "AppID": "", //oluşturduğumuz application'un ID'si.
 "status": "", //ilk satırda yazacak olan mesaj.
 "small_image": "", //small olarak yüklediğimiz fotoğrafa koyduğumuz ad.
 "small_text": "", //small resime imleç geldiğinde yazacak olan mesaj.
 "large_image": "", //large olarak yüklediğimiz fotoğrafa koyduğumuz ad.
 "large_text": "", //large resime imleç geldiğinde yazacak olan mesaj.
 "button1": "", // ilk button ad.
 "url1": "", //ilk button URL
 "button2": "", //ikici button ad.
 "url2": "" //ikici button URL
```` 
